angular.module('starter.services', ['ngStorage','ngCordovaOauth'])

.factory('ItemList', function($http, $ionicLoading, $timeout, $localStorage, $q, SERVER) {

  var items = [];
  var productLists;
  var savedItems = [];
  var cartItem = {};
  var showListCard = "";
  var userContacts = { entry: []};

  $localStorage = $localStorage.$default({
    lystData: {},
    userData: {}
  });

  var showLoading = function(){
    $ionicLoading.show({
      //template: '<ion-spinner icon="ripple" class="spinner-light"></ion-spinner><br>' +
      //'<p>Searching across 809 stores...</p>',
      templateUrl: 'templates/loading.html',
      noBackdrop: false
    });
  };

  var showError = function(){
    $ionicLoading.show({
      template: '<p>There was an error processing your requests.</p>',
      noBackdrop: false
    })
  };

  var hideLoading = function(){
    $ionicLoading.hide();
  };

  return {

    init: function(){

      items = [];
      productLists = "";
      showListCard = true;

      if(typeof(Storage) != 'undefined'){
        $localStorage.lystData.showsaved = !!$localStorage.lystData.saveditems;
      }

    },
    all: function() {

      return items;

    },
    remove: function(item) {

      items.splice(items.indexOf(item), 1);
      //console.log(items.length);

      if(items == undefined || items.length == 0 ){

        showListCard = true;
        $localStorage.lystData.showsaved = false;

      }

    },
    add: function(item_name){

      if(items.indexOf(item_name) == -1){
        items.push(item_name);
        showListCard = items.length <= 0;
      }
      //console.log(items);

    },
    getItems: function(list_object,list_title){

      showLoading();

      if(list_title !== undefined && list_object.title === undefined){
        list_object.title = list_title;
      }

      if(items !== undefined && items.length <= 0){
        for(var q = 0; q < list_object.items.length; q++){
          items.push(list_object.items[q].name);
        }
      }

      //console.log("This is the list Object: ",list_object);

      var formData = new FormData();
      formData.append("userId", "");
      formData.append("list", JSON.stringify(list_object));

      return $http.post(SERVER.graph_url+"/getResults", formData, {headers: {'Content-Type': undefined}})
        .then(function(data){

          hideLoading();
          console.log(data.data.result);
          productLists = data.data.result;

        },function(error){

          hideLoading();
          showError();

          $timeout(function(){
            hideLoading();
          }, 1000);

          //console.log(error);
        });
    },
    getRecommendations: function(list_object){

      showLoading();

      //console.log("This is the list Object: ",list_object);

      items = [];
      for(var i = 0; i < list_object.items.length; i++ ){
        items.push(list_object.items[i].name);
      }

      var formData = new FormData();
      formData.append("userId", "");
      formData.append("list", list_object);

      return $http.post(SERVER.graph_url+"/getResults", formData, {headers: {'Content-Type': undefined}})
        .then(function(data){

          hideLoading();
          //console.log(data.data.result);
          productLists = data.data.result;

        },function(error){

          hideLoading();
          showError();
          $timeout(function(){
            hideLoading();
          }, 1000);


          //console.log(error);
      });
    },
    getProducts: function() {

        return productLists;

    },
    getCartItem: function(){
      return cartItem;
    },
    setShareItem: function(cartItemIndex){
      cartItem = $localStorage.lystData.savedCartItems[cartItemIndex];
    },
    saveList: function(title,itemArray){

      if($localStorage.lystData.saveditems && $localStorage.lystData.saveditems.length > 0){

        savedItems = $localStorage.lystData.saveditems;
        $localStorage.lystData.showsaved = true;

      }

      if (!!itemArray && itemArray.length > 0){

        var tempList = {title: '', items: []};

        for(var item = 0; item < itemArray.length; item++){
          var tempItem = {name: ''};
          tempItem.name = itemArray[item];
          tempList.items.push(tempItem);
        }

        if (title !== undefined || title !== ''){
          tempList.title = title;
        }

        savedItems.push(tempList);
        //console.log(savedItems);
        $localStorage.lystData.saveditems = savedItems;

      } else {
        //console.log('Nothing to be saved my friend.');
      }

    },
    saveCart: function(cart){

      var temp_cart = {};
      if($localStorage.lystData.savedCartItems && $localStorage.lystData.savedCartItems.length > 0){

        temp_cart.items = items;
        temp_cart.lystProducts = cart;
        $localStorage.lystData.savedCartItems.push(temp_cart);

      } else {

        $localStorage.lystData.savedCartItems = [];
        temp_cart.items = items;
        temp_cart.lystProducts = cart;
        $localStorage.lystData.savedCartItems.push(temp_cart);

      }
    },
    deleteCartItem: function(index){
      var temp_items = $localStorage.lystData.savedCartItems;
      temp_items.splice(index, 1);
      if(temp_items !== undefined || temp.length > 0){
        $localStorage.lystData.savedCartItems = temp_items;
      }
    },
    deleteList: function(listIndex){

      var temp = $localStorage.lystData.saveditems;
      //console.log('Before deletion: '+ temp);
      temp.splice(listIndex, 1);
      //console.log('After deletion: '+ temp);

      if (temp !== undefined || temp.length > 0){

        $localStorage.lystData.saveditems = temp;

      }

    },
    getGoogleContacts: function(){

      if(!!$localStorage.lystData.accessToken){

        return $http.get(SERVER.gc_url,{
          headers: {
            "Authorization": "Bearer "+ $localStorage.lystData.accessToken
          }
        }).success(function(response){

          //console.log('Contacts Pulled:', response);

          var entries = response.feed.entry;

          for(var i = 0; i < entries.length; i++){
            // //console.log(entries[i]);
            // //console.log(entries[i].gd$email[0].address);

            if(entries[i].gd$email[0].address  && entries[i].gd$email[0].address !== ""){
              userContacts.entry.push(entries[i]);
            }

          }

          // userContacts.entry = entries;
          $localStorage.userData.userContacts = userContacts;

        }).error(function(error){
          alert(JSON.stringify(error));
        })
      }
    },
    getTemp: function(){
      return JSON.stringify(tempResult);
    }

  };
})

.factory('Contacts', function($q, $ionicLoading, $cordovaContacts, SERVER, $http){

    var showLoading = function(){
      $ionicLoading.show({
        template: '<ion-spinner icon="ripple" class="spinner-light"></ion-spinner><br>' +
        '<p>Fetching contacts...</p>',
        //templateUrl: 'templates/loading.html',
        noBackdrop: false
      });
    };

    var showError = function(){
      $ionicLoading.show({
        template: '<p>There was an error fetching your contacts.</p>',
        noBackdrop: false
      })
    };

    var hideLoading = function(){
      $ionicLoading.hide();
    };

    return {

      getContacts: function(){

        var def = $q.defer();
        var tempContacts = [];

        $cordovaContacts.find({filter: '',fields: ['displayName','emails','photos']}).then(function(response){

          for(var i = 0; i < response.length; i++){

            var tempObject = {};

            if(response[i].emails !== null && response[i].displayName !== null){

              tempObject.displayName = response[i].displayName;
              tempObject.email = response[i].emails[0].value;

              if(response[i].photos !== null){
                tempObject.photo = response[i].photos[0].value;
              }

              tempContacts.push(tempObject);
            }

          }

          //console.log(tempContacts);
          def.resolve(tempContacts);

        }, function(error){

          showError();

          $timeout(function(){
            hideLoading();
          }, 1000);
          //console.log('ERROR: '+ error);

          def.reject(error);

        });

        return def.promise;
      },
      getFriends: function(friendsArrayList){

        console.log(friendsArrayList);

        var friendsData = new FormData();
        friendsData.append('friendsList', friendsArrayList);

        return $http.post(SERVER.graph_url+"/getFriends",friendsData,{headers: {'Content-Type': undefined}});

      }
    }

  })

.factory('Users', function($http, $ionicLoading, $timeout, $localStorage, SERVER){

  var tempResult;

  var graphSignUp = function(username,email,password,device_token){

    var formData = new FormData();
    formData.append('username', username);
    formData.append('emailAddress', email);
    formData.append('password',password);
    formData.append('deviceToken', device_token);

    $http.post(SERVER.graph_url+"/signup",formData,{headers: {'Content-Type': undefined}})
      .success(function(data){
      if (data.status == 3){
        //console.log("User was created. Status: ", data.status);
      }
    }).error(function(error){
      //console.log("Something went wrong in creating the user");
    })


  };

  var graphSignIn = function(email,password){

    var formData = new FormData();
    formData.append('emailAddress', email);
    formData.append('password',password);

    $http.post(SERVER.graph_url+"/signin", formData, {headers: {'Content-Type': undefined}})
      .success(function(data){
      if (data.status == 3){
        //console.log("Sign in was successful. Status: ", data.status);
      }
    }).error(function(error){
      //console.log("Something went wrong when signing in.");
    })

  };

  var sendNotification = function(sender,recipient,list){

    var formData = new FormData();
    formData.append('senderName',sender);
    formData.append('recipientId', recipient);
    formData.append('listToSend', list);

    return $http.post(SERVER.graph_url+"/sendList",formData,{headers: {'Content-Type': undefined}})
      .success(function(data){

        //console.log(data);

        var sender_email = !!$localStorage.userData.userProfile ? $localStorage.userData.userProfile[0].email : "User Email Not Set";
        // var sender_email = "lizagyei@gmail.com";
        // var listName = "Sweet Tooth";

        var shareForm = new FormData();
        shareForm.append("userId", sender_email);
        shareForm.append("listName",list.title);
        // shareForm.append("listName",listName);
        shareForm.append("otherUserId", recipient);

        $http.post(SERVER.graph_url+"/shareList",shareForm,{headers: {'Content-Type': undefined}})
          .success(function(data){
            //console.log("Share response is :",data);
          }).error(function(error){
          //console.log("Error in sharing list :",error);
        })

      }).error(function(error){
        //console.log("There was an error in sending push notification.");
      })
  };

  return {

    getUserDetails: function(){
      return $localStorage.userData.userProfile;
    },
    setAccessToken: function(accessToken){
      $localStorage.lystData.accessToken = accessToken;
    },
    getFacebookUser: function(){

      if(!!$localStorage.lystData.accessToken){

        var uData = [];

        return $http.get(SERVER.f_url, {params: {access_token: $localStorage.lystData.accessToken,
          fields: "id,name,email,gender,picture", format: "json"}}).then(function(response){

          var f_data = response.data;
          var full_name = response.data.name;

          tempResult = {
            uid: f_data.id,
            first_name: full_name.split(" ")[0],
            last_name: full_name.split(" ")[full_name.split(" ").length - 1],
            email: f_data.email,
            image: f_data.picture.data.url
          };

          uData.push(tempResult);
          $localStorage.userData.userProfile = uData;

          if($localStorage.userData.isUser !== undefined){

            $localStorage.userData.isUser = true;
            graphSignIn(tempResult.email,tempResult.uid);

          } else {

            $localStorage.userData.isUser = true;
            graphSignUp(tempResult.first_name,tempResult.email,tempResult.uid,$localStorage.userData.deviceToken);

          }

        },function(error){
          alert(angular.toJson(error));
        })
      }
    },
    getGoogleUser: function(){

      if(!!$localStorage.lystData.accessToken){

        var uData = [];

        return $http({
          method: 'GET',
          url: SERVER.g_url + "?access_token=" + $localStorage.lystData.accessToken
        }).success(function(response){

          tempResult = {
            uid: response.id,
            first_name: response.name.givenName,
            last_name: response.name.familyName,
            email: response.emails[0]["value"],
            image: response.image.url
          };

          uData.push(tempResult);
          $localStorage.userData.userProfile = uData;

          //console.log(tempResult);

          if($localStorage.userData.isUser == undefined){

            graphSignUp(tempResult.first_name,tempResult.email,tempResult.uid,$localStorage.userData.deviceToken);
            $localStorage.userData.isUser = true;

          } else {

            $localStorage.userData.isUser = true;
            graphSignIn(tempResult.email,tempResult.uid);

          }

        }).error(function(error){
          alert(JSON.stringify(error));
        })
      }
    },
    collaborate: function(sender,recipient,list){
      sendNotification(sender,recipient,list);
    }

  };
})

.factory('AutoComplete', function($http, SERVER){

  return {
    get: function(searchTerm){

      var formData = new FormData();
      formData.append("searchText", searchTerm);

      return $http.post(SERVER.graph_url+"/autocomplete",formData,{headers: {'Content-Type': undefined}})
        .then(function(data){
          return data;
        }, function(error){
          //console.log(error);
        });
    }
  }

})

.factory('EmailNotification', function($http, SERVER){

  return {

    sendCheckOutEmail: function(cartItems,totalPrice,email){

      var formData = new FormData();
      formData.append("cartItems", cartItems);
      formData.append("totalPrice", totalPrice);
      formData.append("email", email);

      return $http.post(SERVER.graph_url+"/checkout",formData,{headers: {'Content-Type': undefined}})
        .then(function(data){
          console.log('Email notification sent.');
        }, function(error){
          console.log('Error sending email notification.');
        });
    }
  }
})

.factory('Chat', function($http, $ionicLoading, $localStorage, SERVER){

  var compiledList = [];

  return {
    processText: function(messageText){

      return $http.get(SERVER.graph_url+"/chatbot?textQuery="+messageText,{headers: {'Content-Type': undefined}})
        .then(function (data) {

          var resp = data.data.parameters;
          var speechText = data.data.fulfillment.speech;
          var temp_data = [];

          console.log(resp);

          if(resp["item-ext"] !== ""){

            // console.log("This is my type: ", typeof resp["item-ext"]["items"]);

            if (typeof resp["item-ext"] === "string"){

              temp_data.push(resp["item-ext"]);

            } else if (typeof resp["item-ext"]["items"] === "string"){

              temp_data.push(resp["item-ext"]["items"]);

            } else {

              for (var i = 0; i < resp["item-ext"]["items"].length; i++){
                temp_data.push(resp["item-ext"]["items"][i]);
              }

            }

          }

          if (resp["items"] !== ""){
            temp_data.push(resp["items"]);
          }

          compiledList = temp_data;

          return speechText;

        }, function(error){
          console.log("Error processing request: ",error);
        });

    },
    fetchResults: function(){
      return compiledList;
    }
  }
})

.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      $timeout(function() {
        element[0].focus();
      }, 100);
    }
  };
})

// filters
.filter('nl2br', ['$filter',
  function($filter) {
    return function(data) {
      if (!data) return data;
      return data.replace(/\n\r?/g, '<br />');
    };
  }
]);
