/**
 * Created by elartey on 8/30/16.
 */

angular.module('starter.directives', ['ngStorage','ngCordovaOauth'])

.directive('typeahead', function($timeout){
  return {
    restrict: 'AEC',
    scope: {
      items: '=',
      prompt: '@',
      title: '@',
      model: '=',
      onSelect: '&amp;'
    },
    link: function (scope,elem,attrs) {
      
    },
    templateUrl: 'templates/autocomplete.html'
  };
});
