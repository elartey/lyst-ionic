angular.module('starter.controllers', ['ionic','ngAnimate','ngCordovaOauth','ngCordova','ngStorage','starter.services'])

.controller('TabCtrl', function($scope, $state, $ionicSideMenuDelegate, $localStorage, ItemList, Users, $ionicModal, $cordovaOauth,
                                SERVER, $rootScope){
    $scope.openMenu = function(){
      $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.logOut = function(){

      $localStorage.lystData.accessToken = "";
      $localStorage.userData.userProfile = {};
      $scope.userDetails = !$scope.userDetails;
      $state.go('tab.home');

    };

    $scope.$on("$ionicView.enter", function(){
      $scope.$apply(function(){
        $rootScope.userObject = Users.getUserDetails();
        $rootScope.userImage = !!$rootScope.userObject ? $rootScope.userObject[0].image : undefined;
        $rootScope.userName = !!$rootScope.userObject ? $rootScope.userObject[0].first_name + " " +  $rootScope.userObject[0].last_name : undefined;

        //console.log($rootScope.userImage);
        //console.log($rootScope.userName);
      });
    });

    $scope.facebookLogin = function(){

      $cordovaOauth.facebook(SERVER.f_CLIENT_ID,["email","public_profile","user_friends"]).
        then(function(result){

          Users.setAccessToken(result.access_token);
          Users.getFacebookUser();

        }, function(error){
          alert(JSON.stringify(error));
        });
    };

    $scope.googleLogin = function(){

      $cordovaOauth.google(SERVER.g_CLIENT_ID,["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/plus.me","https://www.googleapis.com/auth/contacts.readonly"]).then(function(result){

        Users.setAccessToken(result.access_token);
        Users.getGoogleUser();

      }, function(error){
        alert(JSON.stringify(error));
      });
    };


  })

.controller('ProfileCtrl', function($scope, $ionicSideMenuDelegate, $localStorage, ItemList, Users){

    $scope.openMenu = function(){
      $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.userDetails = Users.getUserDetails();

    $scope.showProfile = $localStorage.userData.userProfile !== undefined;

    $scope.$on("$ionicView.enter", function(scopes,states){
      $scope.$apply(function(){
        $scope.userDetails = Users.getUserDetails();
      });
    });

  })

.controller('HomeCtrl', function($scope, $state, $timeout, $ionicLoading, $ionicPopup, $ionicModal, $ionicHistory,
                                 $localStorage, $cordovaOauth, ItemList, $ionicSideMenuDelegate,
                                 $rootScope, Contacts, Users, $ionicPlatform, AutoComplete) {


    $scope.$on("$ionicView.enter", function(){

      if( $ionicPlatform.is('android') ) {

        $rootScope.phoneContacts === undefined ? $rootScope.phoneContacts = [] : $rootScope.phoneContacts;

        $timeout(function () {

          if ($rootScope.phoneContacts.length < 1) {

            Contacts.getContacts().then(function (result) {
              $rootScope.phoneContacts = result;

              var contactArray = [];

              angular.forEach($rootScope.phoneContacts, function(person){
                contactArray.push('"'+ person.email + '"');
              });

              Contacts.getFriends(contactArray).then(function(resp){

                $rootScope.friendsData = resp.data;
                console.log($rootScope.friendsData);

                angular.forEach($rootScope.phoneContacts, function(item){
                  angular.forEach($rootScope.friendsData, function(friend){
                    console.log(item);
                    console.log(friend);
                    if(item.email === friend.email){
                      item.isUser = true;
                      console.log(item);
                    } else {
                      console.log("User is not in graph.");
                    }
                  })
                });

              });


            }, function (error) {
              console.log('ERROR: ' + error);
            });

          } else {
            //console.log('Running in browser.');
          }

        }, 500);
      }

        ItemList.init();
        $rootScope.listObject = {title: ''};
        $scope.userDetails = Users.getUserDetails();
        $scope.showWelcome = true;
        $scope.cartItemsList = $localStorage.lystData.savedCartItems;
        $scope.savedItemsList = $localStorage.lystData.saveditems;
        $scope.showSavedItems = !!$scope.savedItemsList;

        $rootScope.newProducts = [];

        updateCounter();
        $scope.$apply(function(){
          $scope.userDetails = Users.getUserDetails();
          $scope.savedItemsList = $localStorage.lystData.saveditems;
          $scope.items = ItemList.all();
          $scope.showListHeaderCard = ($scope.items.length <= 0);
          $scope.cTotal = getTotal();
        });

    });

    var listTemplates = {

      wedding: {
        items: ['napkins','cup cakes','drinks','beers'],
        item_count: 4
      },
      baby: {
        items: ['baby wipes','baby food','weaning spoons','diapers'],
        item_count: 4
      },
      school: {
        items: ['coloring pens & markers','erasers','laptops','notepads'],
        item_count: 4
      },
      office: {
        items: ['highlighters','paper clips','copy paper','fax paper','file folders','letter envelopes','staples'],
        item_count: 7
      }

    };

    $scope.wedding_count = listTemplates.wedding.item_count;
    $scope.baby_count = listTemplates.baby.item_count;
    $scope.school_count = listTemplates.school.item_count;
    $scope.office_count = listTemplates.office.item_count;

  // Modal and helper functions for creating a list

    $scope.pdID = '';

    $scope.showCreateListModal = function(template){
      $ionicModal.fromTemplateUrl(template,{
        scope: $scope,
        animation: 'animated slideInUp'
      }).then(function(modal){
        $scope.modal = modal;
        $scope.modal.show();
      });
    };

    $scope.showCreateList = function(){
    $scope.showCreateListModal('templates/list-modal.html');
  };

    $scope.closeCreateList = function(){
      $scope.modal.hide();
      $scope.modal.remove();
      $scope.showListHeaderCard = ($scope.items.length <= 0);
    };

    $scope.showResultsModal = function(template){
      $ionicModal.fromTemplateUrl(template,{
        scope: $scope,
        animation: 'animated slideInUp'
      }).then(function(modal1){
        $scope.modal1 = modal1;
        $scope.modal1.show();
      });
    };

    $scope.showResults = function(){

      $scope.products = ItemList.getProducts();

      for(var i = 0; i < $scope.items.length; i++){

        var itemKey = $scope.items[i];

        if($scope.products !== "" && $scope.products !== undefined){

          $rootScope.newProducts.push($scope.products[itemKey][0]);

        } else if ($rootScope.sharedObject !== undefined) {

          $rootScope.newProducts = $rootScope.sharedObject;
        }

      }

      $scope.showResultsModal('templates/list-detail-modal.html');
    };

    $scope.closeResults = function(){
      $scope.modal1.hide();
      $scope.modal1.remove();
    };

    $scope.saveCurrentListV2 = function(lists){


      if (!!lists && lists.length > 0){

        var cachedItems = $localStorage.lystData.saveditems;
        var savedStatus = true;

        for(var i = 0; i < cachedItems.length; i++ ){

          var temp = [];
          angular.forEach(cachedItems[i].items, function(r){
            temp.push(r.name);
          });

          if(temp.toString().toUpperCase() === lists.toString().toUpperCase()){
            savedStatus = false;
            console.log(savedStatus);
            break;
          }

        }

        if(savedStatus){

          var savePopupAlert = $ionicPopup.alert({
            title: 'Success',
            template: '<div align="center"><p>Your Lyst has been saved. It will show up on the home screen.</p></div>'
          });

          savePopupAlert.then(function() {
            console.log('This is the list being saved...',lists);
            ItemList.saveList($rootScope.listObject !== undefined ? $rootScope.listObject.title : '',lists);
            savePopupAlert.close();
          });
        }

      } else {

        var savePopupError = $ionicPopup.alert({
          title: 'Error',
          template: '<div align="center"><p>There is nothing in your Lyst.</p></div>'
        });

        savePopupError.then(function() {
          savePopupError.close();
        });
      }

    };

    $scope.fetchItems = function(){

      var currentItems = ItemList.all();
      var temp_object = {};
      temp_object.title = $rootScope.listObject !== undefined ? $rootScope.listObject.title : "";
      temp_object.items = [];

      for (var i = 0; i < currentItems.length; i++){
        var temp_item = {};
        temp_item.name = currentItems[i];
        temp_object.items.push(temp_item);
      }

      //console.log(JSON.stringify(temp_object));

      ItemList.getItems(temp_object).then(function(){
        $ionicHistory.clearCache().then(function () {
          $scope.showResults();
        });
      });

      $timeout(function(){
        hideLoading();
      },20000);
    };

    $scope.dumdum = function(value){
      $scope.cardIndex = value;
    };

    $scope.vumvum = function(dataValue){
      //console.log(dataValue);
      $scope.pdID = dataValue;
    };

    $scope.calcTotal = function(){
      $scope.cTotal = 0;
      angular.forEach($rootScope.newProducts, function(product_item){
        if(product_item !== undefined){
          $scope.cTotal = $scope.cTotal + product_item.price;
        }
      });
      //console.log('This is the item passed: ',$scope.cTotal);
      return $scope.cTotal;
    };

    var getTotal = function(){
      $scope.cTotal = 0;
      angular.forEach($rootScope.newProducts, function(product_item){
        if(product_item !== undefined){
          $scope.cTotal = $scope.cTotal + product_item.price;
        }
      });
      //console.log('This is the item passed: ',$scope.cTotal);
      return $scope.cTotal;
    };

    $scope.replaceV2 = function(sliderItem){
      $rootScope.newProducts[$scope.pdID] = sliderItem;
      $timeout(function(){
        $scope.calcTotal();
      })
    };

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      // data.slider is the instance of Swiper
      $scope.slider = data.slider;
    });

    $scope.$on("$ionicSlides.slideChangeStart", function(event, data){
      console.log('Slide change is beginning');
    });

    $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
      // note: the indexes are 0-based
      $scope.activeIndex = data.activeIndex;
      $scope.previousIndex = data.previousIndex;
    });

    $scope.showCheckoutModal = function(template){
      $ionicModal.fromTemplateUrl(template,{
        scope: $scope,
        animation: 'animated slideInRight'
      }).then(function(modal){
        $scope.modal2 = modal;
        $scope.modal2.show();
      });
    };

    $scope.showCheckout = function(){
      // console.log($rootScope.newProducts);
      $scope.showCheckoutModal('templates/checkout.html');
    };

    $scope.closeCheckout = function(){
      $scope.modal2.hide();
      $scope.modal2.remove();
    };

    $scope.saveItemsToCart = function(cart){

      if(!!cart){
        ItemList.saveCart(cart);
      }
    };

    $scope.selected = {name: 'Visa',image: 'img/card_visa.png'};

    $scope.cardOptions = [
      { name: 'Visa',
        image: 'img/card_visa.png'
      },{
        name: 'Mastercard',
        image: 'img/master-card.png'
      }];

    $scope.triggerListInput = function(){
      $timeout(function(){
        angular.element(document.querySelector('#list-input')).triggerHandler('click');
      },100);
    };

    $scope.getListItems = function(lystItem){

      //console.log( typeof lystItem.valueOf() );
      //console.log("Items to be fetched: ", lystItem);

      if (lystItem.title === undefined){

        var temp_object = {};
        temp_object.items = [];
        temp_object.title = "";
        angular.forEach(lystItem, function(item){
          temp_object.items.push({"name": item});
        });

        ItemList.getItems(temp_object).then(function(){
          $ionicHistory.clearCache().then(function(){
            $scope.products = ItemList.getProducts();
            //console.log("Current Items in products object: ", $scope.products);
            $scope.showResults();
          })
        })

      } else {

        ItemList.getItems(lystItem).then(function(){
          $ionicHistory.clearCache().then(function(){
            $scope.products = ItemList.getProducts();
            //console.log("Current Items in products object: ", $scope.products);
            $scope.showResults();
          })
        })
      }
    };

  // ends

  $scope.oOptions = {
    loop: false,
    effect: 'fade',
    speed: 500
  };

  // $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
  //   // data.slider is the instance of Swiper
  //   $scope.slider = data.slider;
  // });
  //
  // $scope.$on("$ionicSlides.slideChangeStart", function(event, data){
  //   console.log('Slide change is beginning');
  // });
  //
  // $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
  //   // note: the indexes are 0-based
  //   $scope.activeIndex = data.activeIndex;
  //   $scope.previousIndex = data.previousIndex;
  // });

    var onboardProcess = function(){
    $ionicLoading.show({
      templateUrl: 'templates/onboard.html',
      noBackdrop: false,
      scope: $scope,
      animation: 'animated fadeIn'
    })
  };

    $scope.hideOnboard = function(){
    $ionicLoading.hide();
  };

    $scope.personArray = function(){

      var pa_Array = [];
      for(var pa = 0; pa < $rootScope.friendsData.length; pa++){
        pa_Array.push(pa);
        console.log(pa);
      }

      return pa_Array.splice(0,$rootScope.friendsData.length);
    };

    $scope.shareLyst = function(){
      $state.go('contacts');
    };

    var hideLoading = function(){
      $ionicLoading.hide();
    };

    $scope.storeCount = 0;

    var updateCounter = function() {
      $scope.storeCount++;
      $timeout(updateCounter, 500);
    };

    $scope.showCounter = function(){
      //console.log($scope.storeCount);
      updateCounter();
      return $scope.storeCount;
    };

    $scope.fetchTemplate = function(templateName){

      var template = '';

      ItemList.init();

      if(templateName == 'wedding'){
        template = listTemplates.wedding.items;
        $rootScope.listObject.title = 'Wedding List';
      }

      if(templateName == 'baby'){
        template = listTemplates.baby.items;
        $rootScope.listObject.title = templateName;
      }

      if(templateName == 'school'){
        template = listTemplates.school.items;
        $rootScope.listObject.title = 'Back to School List';
      }

      if(templateName == 'office'){
        template = listTemplates.office.items;
        $rootScope.listObject.title = 'Office Supplies';
      }


      for (var i = 0; i < template.length; i++){
        $scope.add(template[i]);
      }

      $state.go('tab.list');
    };

    $scope.add = function(item){
      ItemList.add(item);
    };

    $scope.hideLoader = function(){
      hideLoading();
    };

    $scope.createScreenV2 = function(){
      ItemList.add($scope.item_name.name);
      $scope.showListHeaderCard = ItemList.showListCard;
    };

    $scope.remove = function(item) {

      ItemList.remove(item);
      $scope.showListHeaderCard = ($scope.items.length <= 0) ;

    };

    $scope.removeList = function(index) {

      $timeout(function(){

        ItemList.deleteList(index);
        ItemList.init();

        if( $scope.savedItemsList == undefined || $scope.savedItemsList.length < 1 ){
          $scope.showSavedItems = false;
          $localStorage.lystData.showsaved = false;
          //console.log('Saved Items scope status: ' + $scope.showSavedItems);
        }
      }, 250);

    };

    $scope.getRecommendation = function(lystItem){

      //console.log( typeof lystItem.valueOf() );
      //console.log("Items to be fetched: ", lystItem);

      if (lystItem.title === undefined){

        var temp_object = {};
        temp_object.items = [];
        temp_object.title = "";
        angular.forEach(lystItem, function(item){
          temp_object.items.push({"name": item});
        });

        ItemList.getItems(temp_object).then(function(){
          $ionicHistory.clearCache().then(function(){
            $scope.products = ItemList.getProducts();
            //console.log("Current Items in products object: ", $scope.products);
            $state.go('tab.list-detail');
          })
        })

      } else {

        ItemList.getItems(lystItem).then(function(){
          $ionicHistory.clearCache().then(function(){
            $scope.products = ItemList.getProducts();
            //console.log("Current Items in products object: ", $scope.products);
            $state.go('tab.list-detail');
          })
        })
      }
    };

    $scope.item_name = {name: ''};

    $scope.createScreen = function(){
      $state.go('tab.list');
    };

    $scope.logOut = function(){

      $localStorage.lystData.accessToken = "";
      $localStorage.userData.userProfile = {};
      $scope.userDetails = !$scope.userDetails;

    };

    $scope.openMenu = function(){
      $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.checkOut = function(productList){
      //console.log("Product List: ", productList);
      $rootScope.newProducts = productList.lystProducts;
      $state.go('tab.list-detail');
    };

    $scope.deleteCartItem = function(index){
      ItemList.deleteCartItem(index);
    };

    $scope.setCartShareItem = function(index){
      ItemList.setShareItem(index);
    };

    $scope.autoCompleteData = function(query){

      if(query){
        return AutoComplete.get(query);
      } else {
        return [];
      }
    };

    $scope.autoCompleteItem = "";

    $scope.autoCompleteItemOnSelected = function(){
    //console.log('selected= '+ $scope.autoCompleteItem);
    };

    $scope.doneButtonClickedMethod = function (callback) {
    // print out the selected items
    //console.log(callback.selectedItems);
    for(var i = 0; i < callback.selectedItems.length; i++){
      if(callback.selectedItems[i].name){

        ItemList.add(callback.selectedItems[i].name);
        $scope.showListHeaderCard = ItemList.showListCard;

      } else if (callback.selectedItems[i].name === undefined){

        ItemList.add(callback.selectedItems[i]);
        $scope.showListHeaderCard = ItemList.showListCard;
      }
    }

    $scope.autoCompleteItem = {};

    if($scope.showListHeaderCard == false){
      $rootScope.listObject.title = '';
      }

    };

})

.controller('ItemListCtrl', function($scope, $rootScope, $state, $timeout, $ionicLoading, $ionicHistory,
                                     $ionicPopup, $ionicModal, $localStorage, $cordovaOauth, ItemList,
                                     Users, SERVER, $ionicSideMenuDelegate, AutoComplete, Chat, $ionicScrollDelegate ) {

  var aToken = function(){
    return $localStorage.lystData.accessToken;
  };

  $scope.accessToken = aToken();

  var hideLoading = function(){
    $ionicLoading.hide();
  };

  $scope.item_name = {name: ''};

  $scope.items = ItemList.all();

  $rootScope.newProducts = [];

  /* Results Modal Code */

  $scope.showResultsModal = function(template){
    $ionicModal.fromTemplateUrl(template,{
      scope: $scope,
      animation: 'animated slideInUp'
    }).then(function(modal1){
      $scope.modal1 = modal1;
      $scope.modal1.show();
    });
  };

  $scope.showResults = function(){

    $scope.products = [];

    $scope.products = ItemList.getProducts();

    for(var i = 0; i < $scope.items.length; i++){

      var itemKey = $scope.items[i];

      if($scope.products !== "" && $scope.products !== undefined){

        $rootScope.newProducts.push($scope.products[itemKey][0]);

      }
    }

    $scope.showResultsModal('templates/list-detail-modal.html');
  };

  $scope.closeResults = function(){
    $scope.modal1.hide();
    $scope.modal1.remove();
  };

  $scope.fetchChatItems = function(){

    var currentItems = ItemList.all();
    var temp_object = {};
    temp_object.title = $rootScope.listObject !== undefined ? $rootScope.listObject.title : "";
    temp_object.items = [];

    for (var i = 0; i < currentItems.length; i++){
      var temp_item = {};
      temp_item.name = currentItems[i];
      temp_object.items.push(temp_item);
    }

    //console.log(JSON.stringify(temp_object));

    ItemList.getItems(temp_object).then(function(){
      $ionicHistory.clearCache().then(function () {
        $scope.showResults();
      });
    });

    $timeout(function(){
      hideLoading();
    },20000);
  };

  var getTotal = function(){
    $scope.cTotal = 0;
    angular.forEach($rootScope.newProducts, function(product_item){
      if(product_item !== undefined){
        $scope.cTotal = $scope.cTotal + product_item.price;
      }
    });
    return $scope.cTotal;
  };


  /* Ends here */



  $scope.$on("$ionicView.enter", function(scopes,states){

    $scope.$apply(function(){

      $scope.accessToken = aToken();
      $scope.showListHeaderCard = ($scope.items.length <= 0);

      $scope.items = ItemList.all();
      $scope.cTotal = getTotal();
    })

  });

  $scope.$on("$ionicView.beforeEnter", function(){
    $scope.showListHeaderCard = $scope.items.length <= 0;
  });

  $scope.addItem = function(){

    if( !!$scope.item_name.name ){

      ItemList.add($scope.item_name.name);
      $scope.showListHeaderCard = ItemList.showListCard;

      if($scope.showListHeaderCard == false){
        $rootScope.listObject.title = '';
      }

    } else {

      var blankEntry = $ionicPopup.alert({
        title: 'Error',
        template: '<div align="center"><p>Please enter an item name.</p></div>'
      });

      blankEntry.then(function(res){
        blankEntry.close();
      });
    }

  };

  $scope.remove = function(item) {

    ItemList.remove(item);

    $scope.showListHeaderCard = !!($scope.items.length > 0) ?  false : true;

  };

  $scope.saveCurrentListV2 = function(lists){


    if (!!lists && lists.length > 0){

      var savePopupAlert = $ionicPopup.alert({
        title: 'Success',
        template: '<div align="center"><p>Your Lyst has been saved. It will show up on the home screen.</p></div>'
      });

      savePopupAlert.then(function (res) {
        ItemList.saveList($rootScope.listObject !== undefined ? $rootScope.listObject.title : '',lists);
        savePopupAlert.close();
      });
    } else {

      var savePopupError = $ionicPopup.alert({
        title: 'Error',
        template: '<div align="center"><p>There is nothing in your Lyst.</p></div>'
      });

      savePopupError.then(function (res) {
        savePopupError.close();
        //console.log('Nothing was saved.');
      });
    }

    //if (!!$scope.accessToken || $scope.accessToken !== ''){
    //  $scope.accessToken = aToken();
    //}

  };

  $scope.fetchItems = function(){

    var currentItems = ItemList.all();
    var temp_object = {};
    temp_object.title = $rootScope.listObject !== undefined ? $rootScope.listObject.title : "";
    temp_object.items = [];

    for (var i = 0; i < currentItems.length; i++){
      var temp_item = {};
      temp_item.name = currentItems[i];
      temp_object.items.push(temp_item);
    }

    //console.log(JSON.stringify(temp_object));

    ItemList.getItems(temp_object).then(function(){
      $ionicHistory.clearCache().then(function () {
        $state.go('tab.list-detail');
      });
    });

    $timeout(function(){
      hideLoading();
    },20000);
  };

  $scope.showLoginModal = function(template){
    $ionicModal.fromTemplateUrl(template,{
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
      $scope.modal = modal;
      $scope.modal.show();
    });
  };

  $scope.showLogin = function(){
    $scope.showLoginModal('templates/oauth_modal.html')
  };

  $scope.closeLogin = function(){
    $scope.modal.hide();
    $scope.modal.remove();
  };

  $scope.facebookLogin = function(){

    $cordovaOauth.facebook(SERVER.f_CLIENT_ID,["email","public_profile","user_friends"]).
      then(function(result){

        Users.setAccessToken(result.access_token);
        Users.getFacebookUser();
        $scope.closeLogin();

      }, function(error){
        alert(JSON.stringify(error));
      });
  };

  $scope.googleLogin = function(){

    $cordovaOauth.google(SERVER.g_CLIENT_ID,["https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/plus.me"]).then(function(result){

      Users.setAccessToken(result.access_token);
      Users.getGoogleUser();
      $scope.closeLogin();

    }, function(error){
      alert(JSON.stringify(error));
    });
  };

  $scope.openMenu = function(){
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.shareLyst = function(){
    $state.go('contacts');
  };

  $scope.autoCompleteData = function(query){

    if(query){
      return AutoComplete.get(query);
    } else {
      return [];
    }
  };

  $scope.autoCompleteItem = "";

  $scope.autoCompleteItemOnSelected = function(){
    //console.log('selected= '+ $scope.autoCompleteItem);
  };

  $scope.doneButtonClickedMethod = function (callback) {
    // print out the selected items
    //console.log(callback.selectedItems);
    for(var i = 0; i < callback.selectedItems.length; i++){
      if(callback.selectedItems[i].name){
        ItemList.add(callback.selectedItems[i].name);
        $scope.showListHeaderCard = ItemList.showListCard;
      } else if (callback.selectedItems[i].name === undefined){
        ItemList.add(callback.selectedItems[i]);
        $scope.showListHeaderCard = ItemList.showListCard;
      }
    }

    $scope.autoCompleteItem = {};

    if($scope.showListHeaderCard == false){
      $rootScope.listObject.title = '';
    }
  };

  $scope.triggerListInput = function(){
    $timeout(function(){
      angular.element(document.querySelector('#list-input')).triggerHandler('click');
    },100);
  };

  $scope.messages = [
    {
      message: "Hi, I'm Lyst. I can help you with your shopping if you tell me what you're looking for.",
      user_flag: 1,
      image: "img/default_avatar.png",
      items: [],
      default_name: "Lyst",
      date: new Date().getHours() +":" + new Date().getMinutes() + "."+ new Date().getSeconds()
    },
    {
      message: "For example, 'Get me some eggs' or 'I'd like some bread and cheese.' ",
      user_flag: 1,
      image: "img/default_avatar.png",
      items: [],
      default_name: "Lyst",
      date: new Date().getHours() +":" + new Date().getMinutes() + "."+ new Date().getSeconds()
    }
  ];

  $scope.messageText = "";

  var viewScroll = $ionicScrollDelegate.$getByHandle("userMessageScroll");

  $scope.sendMessage = function(msg){

    var temp_msg = {};
    var bot_msg = {};
    var error_bot = {};

    temp_msg.message = msg;
    temp_msg.user_flag = 2;
    temp_msg.image = "img/default_avatar.png";
    temp_msg.items = [];
    temp_msg.default_name = "Anon";
    temp_msg.date = new Date().getHours() +":" + new Date().getMinutes() + "."+ new Date().getSeconds();

    $scope.messages.push(temp_msg);
    $scope.messageText = "";

    $timeout(function(){
      viewScroll.scrollBottom();
    }, 0);

    Chat.processText(msg).then(function(response){

      console.log(response);
      $scope.processedList = Chat.fetchResults();
      console.log($scope.processedList);
      bot_msg.message = response.replace("[","").replace("]","");
      bot_msg.user_flag = 1;
      bot_msg.image = "img/default_avatar.png";
      bot_msg.items = $scope.processedList;
      bot_msg.default_name = "Lyst";
      bot_msg.date = new Date().getHours() +":" + new Date().getMinutes() + "."+ new Date().getSeconds();

      $scope.messages.push(bot_msg);

      console.log("Processed List", $scope.processedList);

      if($scope.processedList !== undefined){
        ItemList.items = [];
        angular.forEach($scope.processedList, function(listItem){
          ItemList.add(listItem);
        });
      }

      $timeout(function(){
        viewScroll.scrollBottom();
      }, 0);

    },function(error){

        error_bot.message = "Sorry I didn't quite get that.";
        error_bot.user_flag = 1;
        error_bot.image = "img/default_avatar.png";
        error_bot.items = [];
        error_bot.default_name = "Lyst";
        error_bot.date = new Date().getHours() +":" + new Date().getMinutes() + "."+ new Date().getSeconds();

        $scope.messages.push(error_bot);
    });

  };

})

.controller('ListRetailCtrl', function($scope, $state, $timeout, $stateParams, $rootScope, $ionicModal,
                                       $ionicPopup, $ionicLoading, $ionicHistory, ItemList, $ionicSideMenuDelegate,
                                       EmailNotification, $localStorage) {

    $scope.pdID = '';
    $scope.sliderItems = '';
    $scope.showNotif = true;
    $scope.cardIndex = '';

    $scope.$on("$ionicView.beforeEnter", function(){

      $scope.products = ItemList.getProducts();
      $scope.items = ItemList.all();
      $rootScope.newProducts = [];
      $rootScope.newProductsV2 = [];
      $scope.cartItemsList = $localStorage.lystData.savedCartItems;

      for(var i = 0; i < $scope.items.length; i++){

        var itemKey = $scope.items[i];
        // console.log("Products: ",$scope.products);

        if($scope.products !== "" && $scope.products !== undefined){

          $rootScope.newProducts.push($scope.products[itemKey][0]);

        } else if ($rootScope.sharedObject !== undefined) {

          $rootScope.newProducts = $rootScope.sharedObject;
        }

      }

    });

    $scope.$on('$ionicView.enter',function(){
    $scope.showListResults = !(!!$scope.products || !!$rootScope.sharedObject);
    $scope.$apply(function(){
      $scope.cTotal = getTotal();
    });
  });

    $scope.options = {
      loop: false,
      effect: 'fade',
      speed: 500
    };

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      // data.slider is the instance of Swiper
      $scope.slider = data.slider;
    });

    $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
      // note: the indexes are 0-based
      $scope.activeIndex = data.activeIndex;
      $scope.previousIndex = data.previousIndex;
    });

    $scope.saveItemsToCart = function(cart){

      if(!!cart){
        ItemList.saveCart(cart);
      }
    };

    $scope.dumdum = function(value){

      $scope.cardIndex = value;
      //$scope.pItems = $scope.products[0];
      //$scope.currentIndex = 0;
      //$scope.productDetails = $scope.products[0][$scope.pdID];
      ////console.log($scope.productDetails);

      //$scope.sliderItems = [];
      //for (i = 0; i < $scope.products.length; i++){
      //  //console.log($scope.products[i][$scope.pdID]);
      //  $scope.sliderItems.push($scope.products[i][$scope.pdID]);
      //}

    };

    $scope.vumvum = function(dataValue){
      //console.log(dataValue);
      $scope.pdID = dataValue;
    };

    $scope.replace = function(cardItem){

      if($scope.cardIndex == '0'){
        $rootScope.newProducts[$scope.pdID] = cardItem;
      }

      if($scope.cardIndex == '1'){
        $rootScope.newProductsV2[$scope.pdID] = cardItem;
      }

    };

    $scope.getCardTotal = function(card){

      $scope.totalPrice = 0;
      $scope.totalPriceV2 = 0;

      var itemLength = $rootScope.newProducts !== undefined ? $rootScope.newProducts.length : 0;
      var itemLengthV2 = $rootScope.newProductsV2 !== undefined ? $rootScope.newProductsV2.length : 0;

      for(var k = 0; k < itemLength; k++){
        if($rootScope.newProducts[k] !== undefined){
          $scope.totalPrice = $scope.totalPrice + $rootScope.newProducts[k].price;
        }
      }

      for(var r = 0; r < itemLengthV2; r++){

        if($rootScope.newProductsV2[r] !== undefined){
          $scope.totalPriceV2 = $scope.totalPriceV2 + $rootScope.newProductsV2[r].price;
        }
      }

      if(card == '1'){
        return $scope.totalPrice;
      } else if (card == '2') {
        return $scope.totalPriceV2;
      }



    };

    $scope.calcTotal = function(){
      $scope.cTotal = 0;
      angular.forEach($rootScope.newProducts, function(product_item){
        if(product_item !== undefined){
          $scope.cTotal = $scope.cTotal + product_item.price;
        }
      });
      //console.log('This is the item passed: ',$scope.cTotal);
      return $scope.cTotal;
    };

    var getTotal = function(){
      $scope.cTotal = 0;
      angular.forEach($rootScope.newProducts, function(product_item){
        if(product_item !== undefined){
          $scope.cTotal = $scope.cTotal + product_item.price;
        }
      });
      //console.log('This is the item passed: ',$scope.cTotal);
      return $scope.cTotal;
    };

    $scope.replaceV2 = function(sliderItem){
      $rootScope.newProducts[$scope.pdID] = sliderItem;
      $timeout(function(){
        $scope.calcTotal();
      })
    };

    $scope.selected = {name: 'Visa',image: 'img/card_visa.png'};

    $scope.cardOptions = [
      { name: 'Visa',
        image: 'img/card_visa.png'
      },{
        name: 'Mastercard',
        image: 'img/master-card.png'
      }];

    $scope.showPriceModal = function(template){
      $ionicModal.fromTemplateUrl(template,{
        scope: $scope,
        animation: 'animated zoomIn'
      }).then(function(modal){
        $scope.modal1 = modal;
        $scope.modal1.show();
      });
    };

    $scope.showPrice = function(){

      $scope.cardName = $scope.items[$scope.pdID];
      //console.log($scope.cardName);
      $scope.sliderItems = '';
      var itemKey = $scope.items[$scope.pdID];

      $scope.sliderItems = $scope.products[itemKey];
      //$scope.showPriceModal('templates/products.html');
      //$scope.sliderOverlay();
      $scope.sliderPopup();

    };

    $scope.closePrice = function(){
      //$scope.modal1.hide();
      //$scope.modal1.remove();
      hideLoading();
    };

    $scope.showCheckoutModal = function(template){
      $ionicModal.fromTemplateUrl(template,{
        scope: $scope,
        animation: 'animated slideInUp'
      }).then(function(modal){
        $scope.modal = modal;
        $scope.modal.show();
      });
    };

    $scope.showCheckout = function(){
      console.log($rootScope.newProducts);
      $scope.showCheckoutModal('templates/checkout.html');
    };

    $scope.closeCheckout = function(){
      $scope.modal.hide();
      $scope.modal.remove();
    };

    $scope.buyCheckout = function(){

      var userEmail = $localStorage.userData.userProfile !== undefined ? $localStorage.userData.userProfile[0].email : null;

      var saveBuyPopup = $ionicPopup.alert({
        title: 'Thank You',
        template: '<div align="center"><p>Thank you for trying out Lyst. This feature will be avaliable in the final release.</p></div>'
      });

      var userCheckoutPopup = $ionicPopup.alert({
        title: 'Thank You',
        template: '<div align="center"><p>Thank you for trying out Lyst. You will receive an email with details of your cart.</p></div>'
      });

      if(userEmail !== null){

        userCheckoutPopup.then(function() {
          $scope.closeCheckout();
          EmailNotification.sendCheckOutEmail($rootScope.newProducts,$scope.cTotal,userEmail);
          userCheckoutPopup.close();
          $state.go('tab.home');
        });

      } else {

        saveBuyPopup.then(function() {
          $scope.closeCheckout();
          saveBuyPopup.close();
          $state.go('tab.home');
        });

      }

    };

    $scope.showItemPopup = function(index){

      var showItemPop = $ionicPopup.alert({
        scope: $scope,
        title: 'Details',
        templateUrl: 'templates/item-detail.html'
      });

      showItemPop.then(function(res){
        showItemPop.close();
      });
    };

    $scope.homeScreen = function(){

      ItemList.init();
      $state.go('tab.home');

    };

    $scope.createList = function(){

      $state.go('tab.list');

    };

    $scope.openMenu = function(){
      $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.swiper = {};

    $scope.onReadySwiper = function (swiper) {

      swiper.on('onTransitionEnd', function(e){
        console.log('transition ended',e);
        $timeout(function(){
          getTotal();
        },100);

      });

    };

    // Migrated functions

    $scope.getRecommendation = function(lystItem){

      if (lystItem.title === undefined){

        var temp_object = {};
        temp_object.items = [];
        temp_object.title = "";
        angular.forEach(lystItem, function(item){
          temp_object.items.push({"name": item});
        });

        ItemList.getItems(temp_object).then(function(){
          $ionicHistory.clearCache().then(function(){
            $scope.products = ItemList.getProducts();
            //console.log("Current Items in products object: ", $scope.products);
            $state.go('tab.home');
          })
        })

      } else {

        ItemList.getItems(lystItem).then(function(){
          $ionicHistory.clearCache().then(function(){
            $scope.products = ItemList.getProducts();
            //console.log("Current Items in products object: ", $scope.products);
            $state.go('tab.home');
          })
        })
      }
    };

    $scope.deleteCartItem = function(index){
      ItemList.deleteCartItem(index);
    };

    //ends

})

.controller('ContactsCtrl', function($scope, $state, $ionicSideMenuDelegate, $cordovaContacts, $ionicLoading, ItemList,
                                     $rootScope, $ionicPopup, Users, $localStorage){

    $scope.openMenu = function(){
      $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.contactItem = {};
    $rootScope.listSaved = false;

    $scope.inviteUser = function(index){
      //console.log(index);
      //console.log($rootScope.phoneContacts[index]["email"]);
      var collaborator = $rootScope.phoneContacts[index]["email"];
      var currentUser = $localStorage.userData.userProfile;
      var items = ItemList.getCartItem();
      Users.collaborate(currentUser[0].first_name, collaborator, items);
    };

    $scope.saveList = function(){

      var saveCurrentListPopupAlert = $ionicPopup.alert({
        title: 'Save your Lyst',
        template: '<div align="center"><p>Would you like to save the lyst? It will show up on the home screen.</p></div>',
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function(e){
              var current_list = ItemList.all();
              ItemList.saveList(current_list);
              $state.go('tab.home');
            }
          }
        ]

      });

      saveCurrentListPopupAlert.then(function(){
        //console.log('Saved the lyst!');
        saveCurrentListPopupAlert.close();
      })
    };

    $scope.addPopUp = function(){

      if($localStorage.userData.isUser === undefined){

        var addPeoplePopup = $ionicPopup.alert({
          title: 'Error',
          template: '<div align="center"><p>You have to sign in to use this feature.</p></div>'
        });

        addPeoplePopup.then(function(){
          addPeoplePopup.close();
        });

      } else {
        console.log("User has signed up and in.");
      }
    }

  });

