// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','ion-autocomplete','angular.filter','ksSwiper','ionic.service.core','starter.controllers','starter.services'])

.run(function($ionicPlatform,$ionicScrollDelegate) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard && window.cordova.plugins.inAppBrowser) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    $ionicScrollDelegate.scrollTop();

  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.navBar.alignTitle('center');
  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'TabCtrl'
  })

  // Each tab has its own nav history stack:

  .state('tab.home', {
    cache: false,
    url: '/home',
    views: {
      'tab-home': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('tab.list', {
      cache: true,
      url: '/list',
      views: {
        'tab-list': {
          templateUrl: 'templates/list.html',
          controller: 'ItemListCtrl'
        }
      }
    })

  .state('tab.list-detail', {
    cache: true,
    url: '/lists',
    views: {
      'tab-lists': {
        templateUrl: 'templates/list-detail.html',
        controller: 'ListRetailCtrl'
      }
    }
  })

  .state('profile', {
    url: '/profile',
    controller: 'ProfileCtrl',
    templateUrl: 'templates/profile.html'
  })

  .state('login', {
    url: '/login',
    controller: 'TabCtrl',
    templateUrl: 'templates/oauth.html'
  })

  .state('contacts',{
    cache: true,
    url: '/contacts',
    controller: 'ContactsCtrl',
    templateUrl: 'templates/contacts.html'
  })

  .state('history',{
    url: '/history',
    controller: 'HomeCtrl',
    templateUrl: 'templates/history.html'
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/home');

})

.constant('SERVER',{

      // Endpoint to fetch items
      graph_url: 'http://rndvu.herokuapp.com/2.0a/lyst',
      graph_test: 'http://localhost:7070/2.0a/lyst',
      f_CLIENT_ID: "1030093513713194",
      f_url: "https://graph.facebook.com/v2.2/me",
      g_CLIENT_ID: "360716051075-s52qdekk8e10pm3mhbtc0hptim7m3tvn.apps.googleusercontent.com",
      g_API_KEY: "AIzaSyB1TnCbV2rt_OVdncoC9Fynrhpf1MqFl2s",
      g_url: "https://www.googleapis.com/plus/v1/people/me",
      gc_url: "https://www.google.com/m8/feeds/contacts/default/full?alt=json&max-results=400&v=3.0"
  
})
.run(function($ionicPlatform, ItemList, $state, $ionicLoading, $timeout, $rootScope, $localStorage, $ionicPopup) {

  $ionicPlatform.ready(function () {

    var showFriendNotif = function(){
      $ionicLoading.show({
        template: "<p>Your friend " + $rootScope.sender +
        " wants you to help with this list.</p>",
        noBackdrop: false
      })
    };

    var hideNotif = function(){
      $ionicLoading.hide();
    };


    var push = new Ionic.Push({

      "debug": true,
      "onNotification": function(notification){

        var item_payload = notification.payload.items;
        var friend = notification.payload.sender;
        var current_items = ItemList.all();

        $rootScope.sender = friend;

        if(current_items.length > 0){
          ItemList.init();
        }

        console.log(item_payload.lystProducts);

        $rootScope.sharedObject = item_payload.lystProducts;

        var showCollaborate = $ionicPopup.alert({
          title: 'Collaborate',
          template: "<div align='center'><p>Your friend "+$rootScope.sender+" wants you to help with a list</p></div>",
          scope: $rootScope,
          buttons: [
            {text: 'Cancel'},
            {
              text: '<b>View</b>',
              type: 'button-positive',
              onTap: function(e){
                angular.forEach(item_payload.items, function(item){
                  ItemList.add(item);
                });

                if(item_payload.lystProducts === undefined){
                  $state.go('tab.list');
                } else {
                  $state.go('tab.list-detail');
                }
              }
            }
          ]

        });

        var confirmCollaboration = function () {
          showCollaborate.then(function(){
            showCollaborate.close();
          });
        };

        $timeout(function(){
          confirmCollaboration();
          $rootScope.$apply();
        },2000);
      }
    });

    console.log(push);

    push.register(function(token){
      console.log("Device Token:",token.token);
      $localStorage.userData.deviceToken = token.token;
    });
    //
    // Contacts.getFriends(testArray).then(function(resp){
    //
    //   $rootScope.friendsData = resp.data;
    //
    //   console.log($rootScope.friendsData);
    //
    // });

  });

});
